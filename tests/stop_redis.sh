#!/usr/bin/env bash

sleep 10
docker stop rq_api
docker stop rq_redis
docker network rm lvcsrd_default
