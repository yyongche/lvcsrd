#!/usr/bin/env bash

rm -f conf/lvcsr.log
rm -rf input/cooky
rm -rf input/koya
rm -rf output/cooky
rm -rf output/koya

find wip/ -mindepth 1 -maxdepth 1 -type d -exec rm -rf {} \;
