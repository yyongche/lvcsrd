#!/bin/bash

function jq_rm {
    expdirs=(`jq .[].kws_expdir $1`)
    if [[ ! "${expdirs[*]}" =~ "`basename $2`" ]]; then
        rm -rf $2
    fi
}
export -f jq_rm

case "$1" in
    hddin)
        if [ "$#" -ne 2 ] || ! [ -d "$2" ]; then
            echo "Usage: $0 hddin <indir>"
            exit 1
        fi
        find $2 -mindepth 1 -maxdepth 1 -mtime +2 -exec rm -rf {} \;
        ;;
    hdd)
        if [ "$#" -ne 2 ] || ! [ -d "$2" ]; then
            echo "Usage: $0 hdd <workdir>"
            exit 1
        fi
        find $2 -mindepth 1 -maxdepth 1 -mmin +30 -exec rm -rf {} \;
        ;;
    hddexp)
        if [ "$#" -ne 3 ] || ! [ -f "$2" ] || ! [ -d "$3" ]; then
            echo "Usage: $0 hddexp <stores.json> <expdir>"
            exit 1
        fi
        find $3 -mindepth 1 -maxdepth 1 -name exp_* -type d \
            -exec bash -c 'jq_rm "$@"' bash $2 {} \;
        ;;
    mnt)
        df 2>&1 | grep endpoint | awk '{ print substr($2,1,length($2)-1) }' | \
            xargs -n 1 -r fusermount -u
        ;;
    *)
        echo "Usage: $0 (hddin|hdd|hddexp|mnt) [options]"
        exit 1
esac
