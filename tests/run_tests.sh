#!/usr/bin/env bash

cp tests/cooky.wav input/cooky/$(uuidgen).wav
mkdir -p input/cooky/a/b
cp tests/cooky.wav input/cooky/a/b/$(uuidgen).wav
cp tests/koya.wav input/koya/$(uuidgen).wav
