#!/usr/bin/env bash

docker network create lvcsrd_default
docker run -it --rm -p 6379:6379 -d \
    --network lvcsrd_default --name rq_redis \
    redis:6

APPIMAGE_HOME_DIR=$( dirname -- "$0"; )/LvcsrDApp-x86_64.AppImage.home
if [ -d ${APPIMAGE_HOME_DIR} ]; then

docker run -it --rm -p 8000:8000 -d \
    -e LISTEN_PORT="8000" \
    -e RQ_DASHBOARD_REDIS_URL="redis://rq_redis:6379" \
    -e LVCSR_DAEMON_CONFDIR="/apprun/conf" \
    -v ${APPIMAGE_HOME_DIR}/conf:/apprun/conf \
    --network lvcsrd_default --name rq_api \
    lvcsrd-api:0.1

else

docker run -it --rm -p 8000:8000 -d \
    -e LISTEN_PORT="8000" \
    -e RQ_DASHBOARD_REDIS_URL="redis://rq_redis:6379" \
    -e LVCSR_DAEMON_CONFDIR="/apprun/conf" \
    -v $(pwd)/conf:/apprun/conf \
    --network lvcsrd_default --name rq_api \
    lvcsrd-api:0.1

fi
