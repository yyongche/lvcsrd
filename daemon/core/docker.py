#!/usr/bin/env python3

from collections import OrderedDict
import subprocess  # nosec


def run_stages(stages, config, wipdir, expdirs):
    stages = [s for s in stages if s in WORK_STAGES]
    for work_stage, work_func in WORK_STAGES.items():
        if work_stage in stages:
            stages.remove(work_stage)
            if work_stage.startswith("vad") and work_func is not None:
                kind = work_stage[4:-1]
                work_func(kind, config, wipdir, expdirs[work_stage])
            else:
                work_func(config, wipdir, expdirs[work_stage])
            break
    return stages


def run_docker(wipdir, expdir, image, **kwargs):
    command = ["docker", "run", "--rm"]

    command.append("-v")
    command.append(f"{str(wipdir)}:/apprun/wip")
    command.append("-v")
    command.append(f"{str(expdir)}:/apprun/exp")
    if "basedir" in kwargs:
        command.append("-v")
        command.append(f"{str(kwargs['basedir'])}:/apprun/base")

    if "gpu" in kwargs and kwargs["gpu"]:
        command.append("--gpus")
        command.append("all")
    command.append(image)
    if "kind" in kwargs:
        command.append(kwargs["kind"])

    with open(str(wipdir / "run_docker.log"), "at", encoding="utf-8") as log:
        subprocess.run(  # nosec
            command, stdout=log, stderr=subprocess.STDOUT, check=False
        )


def run_stt(_, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-stt:0.1")


def run_kws(_, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-kws:0.1")


def run_vad(kind, _, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-vad:0.1", gpu=True, kind=kind)


def run_3ws(_, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-3ws:0.1")


def run_sud(_, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-sud:0.1")


def run_ta(_, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-text-analysis:0.1")


def run_ae(_, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-audio-enhance:0.1", gpu=True)


def run_cskwlm(basedir, _, wipdir, expdir):
    run_docker(wipdir, expdir, "lvcsr-cskwlm:0.1", basedir=basedir)


WORK_STAGES = OrderedDict(
    [
        ("enhance", run_ae),
        ("vad", None),
        ("vad[hf]", run_vad),
        ("vad[atc]", run_vad),
        ("vad[vuhf]", run_vad),
        ("stt", run_stt),
        ("kws", run_kws),
        ("ta", run_ta),
        ("3ws", run_3ws),
        ("sud", run_sud),
    ]
)
