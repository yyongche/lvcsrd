#!/usr/bin/env python3

from pathlib import Path
from typing import List, Mapping
import logging

from watchdog.events import PatternMatchingEventHandler

from .pipeline import Pipeline


class Watcher(PatternMatchingEventHandler):

    def __init__(self, extensions: List[str], indir: Path,
                 pipelines: Mapping[str, Pipeline]):
        super().__init__(patterns=extensions, ignore_directories=True)
        self.indir = indir
        self.pipelines = pipelines

    def on_created(self, event):
        srcfile = Path(event.src_path).relative_to(str(self.indir))
        paths = list(srcfile.parents)
        if len(paths) >= 2 and str(paths[-2]) in self.pipelines:

            logging.info("File \"%s\" has been detected.", event.src_path)
            self.pipelines[str(paths[-2])].work_on(Path(event.src_path))

        logging.debug("File \"%s\" has been created.", event.src_path)
        return super().on_created(event)
