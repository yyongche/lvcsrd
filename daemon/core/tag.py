#!/usr/bin/env python3

import json
import re


class Tagger:

    def __init__(self, confdir):
        self.conf = {}
        self._enable = False

        configfile = confdir / "@lvcsr.tagger.json"
        if configfile.exists():
            with open(str(configfile), "rt", encoding="utf-8") as config:
                try:
                    self.conf = json.load(config)
                    self.init_config()
                    self._enable = True
                except json.JSONDecodeError:
                    self._enable = False

    @property
    def enable(self):
        return self._enable

    @property
    def suffix(self):
        return ".tagger.txt"

    def init_config(self):
        for param in ["fillers", "partials", "names", "foreigns", "abbrs"]:
            if param not in self.conf:
                self.conf[param] = []
        for param in ["filler", "partial", "name", "foreign", "abbr"]:
            if param not in self.conf:
                self.conf[param] = ""

    @classmethod
    def tag_multiwords(cls, text, tags, sym):
        """tag multiple words, enclosing them in curly brackets '{}'."""
        prefix = "{" + sym
        for tag in tags:
            if tag in text:
                text = re.sub(r"\b" + tag + r"\b", prefix + tag + "}", text)
        return text

    @classmethod
    def tag_singlewords(cls, text, tags, sym):
        """tag single words."""
        for tag in tags:
            if tag in text:
                text = re.sub(r"\b" + tag + r"\b", sym + tag, text)
        return text

    @classmethod
    def tag_abbreviations(cls, text, tags, sym):
        """like tag words but remove space in the middle."""
        prefix = "{" + sym
        for tag in tags:
            if tag in text:
                content = tag.replace(" ", "")
                text = re.sub(r"\b" + tag + r"\b", prefix + content + "}",
                              text)
        return text

    def write_line(self, channel, segment):
        line = " ".join([x.word for x in segment])
        line = self.tag_multiwords(line, self.conf["fillers"],
                                   self.conf["filler"])
        line = self.tag_multiwords(line, self.conf["partials"],
                                   self.conf["partial"])
        line = self.tag_singlewords(line, self.conf["names"],
                                    self.conf["name"])
        line = self.tag_singlewords(line, self.conf["foreigns"],
                                    self.conf["foreign"])
        line = self.tag_abbreviations(line, self.conf["abbrs"],
                                      self.conf["abbr"])

        start = round(segment[0].begin, 2)
        end = round(segment[-1].begin + segment[-1].duration, 2)
        line = f"{start}\t{end}\t{channel}|UNKNOWN|U: {line}\n"
        return line

    def write_text(self, obj, outfile):
        with open(str(outfile), "wt", encoding="utf-8",
                  newline='\r\n') as content:
            for channel, results in obj["channels"].items():
                text = ""
                for segment in results["stt"]:
                    if segment:
                        text += self.write_line(channel, segment)
                content.write(f"{text}")
