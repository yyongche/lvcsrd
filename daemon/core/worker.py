#!/usr/bin/env python3

from pathlib import Path
from typing import Dict, List, Optional
import datetime
import logging
import os
import shutil

from multiprocess import Process  # noqa
from redis import Redis
from rq import Queue, Worker
from rq.command import send_shutdown_command

from .job import run_job


class WorkerProcess(Process):

    def __init__(self, queue: Queue, *, conn: Optional[Redis] = None):

        super().__init__()
        self.conn = Redis() if conn is None else conn
        self.worker = Worker([queue],
                             connection=self.conn,
                             log_job_description=False)

    def run(self):
        self.worker.work(logging_level="WARNING")

    def shutdown(self):
        send_shutdown_command(self.conn, self.worker.name)


class WorkerQueue:

    def __init__(self,
                 pipeline: str,
                 dirs: Dict[str, Path],
                 *,
                 conn: Optional[Redis] = None,
                 num_workers: int = 1):

        super().__init__()
        self.pipeline = pipeline
        self.dirs = dirs

        self.conn = Redis() if conn is None else conn
        self.queue = Queue(self.pipeline, connection=self.conn)
        self.workers = [
            WorkerProcess(self.queue, conn=self.conn)
            for _ in range(num_workers)
        ]

    def start(self):
        for worker in self.workers:
            worker.start()

    def shutdown(self):
        for worker in self.workers:
            worker.shutdown()
            worker.join()

    def get_expdirs(self, stages: List[str]) -> Dict[str, Path]:
        expdirs: Dict[str, Path] = {}
        if self.conn.exists(f"{self.pipeline}_store"):
            for key, value in self.conn.hgetall(
                    f"{self.pipeline}_store").items():
                keystr = key.decode()
                if keystr.endswith("_expdir") and keystr[:-7] in stages:
                    expdir = self.dirs["exp"] / value.decode()
                    if expdir.exists():
                        expdirs[keystr[:-7]] = expdir
        return expdirs

    def add_job(self, stages: List[str], datafiles: List[Path]):
        currtime = datetime.datetime.now()
        wipname = f"data_{os.getpid()}_{currtime:%d%m%Y%H%M%S%f}"

        logging.info("Job \"%s\" has been created in Pipeline \"%s\".",
                     wipname, self.pipeline)

        currdir = self.dirs["wip"] / wipname
        currdir.mkdir(mode=0o775)

        conffiles = self.dirs["conf"].glob("*.conf")
        for conffile in conffiles:
            shutil.copy(str(conffile),
                        str(currdir / f"@lvcsr.{conffile.name}"))
        jsonfiles = self.dirs["conf"].glob("*.json")
        for jsonfile in jsonfiles:
            shutil.copy(str(jsonfile),
                        str(currdir / f"@lvcsr.{jsonfile.name}"))

        miscfiles: List[str] = []
        for miscname in miscfiles:
            miscfile = self.dirs["conf"] / miscname
            if miscfile.exists():
                shutil.copy(str(miscfile),
                            str(currdir / f"@lvcsr.{miscfile.name}"))

        self.queue.enqueue(run_job,
                           kwargs={
                               "pipeline": self.pipeline,
                               "stages": stages,
                               "expdirs": self.get_expdirs(stages),
                               "expdir": self.dirs["exp"],
                               "wipdir": currdir,
                               "indir": self.dirs["in"],
                               "outdir": self.dirs["out"],
                               "datafiles": list(datafiles),
                           },
                           job_timeout=3600)
