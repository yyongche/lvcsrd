#!/usr/bin/env python3

import json


class WordReplacer:

    def __init__(self, confdir):
        self.substitutes = {}
        self._enable = False

        configfile = confdir / "@lvcsr.substitutes.json"
        if configfile.exists():
            with open(str(configfile), "rt", encoding="utf-8") as config:
                try:
                    self.substitutes = json.load(config)
                    self._enable = True
                except json.JSONDecodeError:
                    self._enable = False

    @property
    def enable(self):
        return self._enable

    def replace(self, word):
        if word in self.substitutes:
            return self.substitutes[word]
        return word

    def convert(self, obj):
        for _, results in obj["channels"].items():
            for segment in results["stt"]:
                for word in segment:
                    word.word = self.replace(word.word)
        return obj
