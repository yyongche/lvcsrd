#!/usr/bin/env python3

from collections import defaultdict
from pathlib import Path
from typing import Any, Dict
import configparser
import json
import logging
import shutil

from lvcsr.job import JSONDecoder, JSONEncoder, JobFile
from .format import write_transcripts
from .substitute import WordReplacer
from .tag import Tagger
from .transcribe import Transcriber
from . import docker, sox


def read_config(conffile: Path) -> Dict[str, Any]:
    reader = configparser.ConfigParser()
    reader.read(conffile)

    config: Dict[str, Any] = {}
    config["output_enhance"] = reader.getboolean("pipeline",
                                                 "output_enhance",
                                                 fallback=False)

    return config


def create_kalfiles(pipeline, filename, datadir, indir, outdir, *, index=-1):
    kalfiles = []

    # copy before performing SoX
    datapath0 = datadir / f"{filename.stem}_O{filename.suffix.lower()}"
    if index >= 0:
        datapath0 = datapath0.with_name(f"{index:08d}_O{datapath0.suffix}")
    shutil.copy(str(filename), str(datapath0))
    if datapath0.suffix in [".mp4"]:  # convert to wave file
        datapath0 = sox.convert(datapath0, datapath0.with_suffix(".wav"))

    num_channels = sox.get_num_channels(datapath0)
    if num_channels == 2:

        datapathl1 = datadir / f"{filename.stem}_L{datapath0.suffix}"
        if index >= 0:
            datapathl2 = datapathl1.with_name(
                f"{index:08d}_L{datapathl1.suffix}")
        datapathl0 = datapathl2 if index >= 0 else datapathl1
        sox.extract_channel(datapath0, datapathl0, 1)

        outpath = outdir / filename.with_suffix(".json").relative_to(indir)
        kalfiles.append(
            JobFile(name=datapathl1.name,
                    pipeline=pipeline,
                    channel="L",
                    path=filename,
                    outpath=outpath,
                    datapath=datapathl0,
                    segments=[] if sox.is_valid(datapathl0) else None))

        datapathr1 = datadir / f"{filename.stem}_R{datapath0.suffix}"
        if index >= 0:
            datapathr2 = datapathr1.with_name(
                f"{index:08d}_R{datapathr1.suffix}")
        datapathr0 = datapathr2 if index >= 0 else datapathr1
        sox.extract_channel(datapath0, datapathr0, 2)

        outpath = outdir / filename.with_suffix(".json").relative_to(indir)
        kalfiles.append(
            JobFile(name=datapathr1.name,
                    pipeline=pipeline,
                    channel="R",
                    path=filename,
                    outpath=outpath,
                    datapath=datapathr0,
                    segments=[] if sox.is_valid(datapathr0) else None))

    elif num_channels == 1:

        datapathm1 = datadir / f"{filename.stem}_M{datapath0.suffix}"
        if index >= 0:
            datapathm2 = datapathm1.with_name(
                f"{index:08d}_M{datapathm1.suffix}")
        datapathm0 = datapathm2 if index >= 0 else datapathm1

        bitrate, _, encoding, sample_rate = sox.get_info(datapath0)
        if bitrate != 16 or encoding != "Signed Integer PCM" \
                or sample_rate != 8000:
            sox.resample(datapath0, datapathm0)
        else:
            shutil.copy(str(datapath0), str(datapathm0))

        outpath = outdir / filename.with_suffix(".json").relative_to(indir)
        kalfiles.append(
            JobFile(name=datapathm1.name,
                    pipeline=pipeline,
                    channel="M",
                    path=filename,
                    outpath=outpath,
                    datapath=datapathm0,
                    segments=[] if sox.is_valid(datapathm0) else None))

    return kalfiles


def run_job(**kwargs):
    wipdir = kwargs["wipdir"]
    jobfiles = []

    for i, datafile in enumerate(kwargs["datafiles"]):
        jfiles = create_kalfiles(kwargs["pipeline"],
                                 datafile,
                                 wipdir,
                                 kwargs["indir"],
                                 kwargs["outdir"],
                                 index=i)
        for jfile in jfiles:
            jfile.outpath.parent.mkdir(exist_ok=True, parents=True)
            jfile.outpath.parent.chmod(0o777)
            jobfiles.append(jfile)

    conffile = wipdir / "@lvcsr.pipeline.conf"
    config = read_config(conffile)
    resfile = wipdir / "@lvcsr.files.json"
    with open(str(resfile), "wt", encoding="utf-8") as result:
        json.dump(jobfiles, result, cls=JSONEncoder)

    stages = kwargs["stages"]
    expdirs = defaultdict(lambda: kwargs["expdir"], kwargs["expdirs"])
    while stages:
        stages = docker.run_stages(stages, config, wipdir, expdirs)

        with open(resfile, "rt", encoding="utf-8") as result:
            jobfiles = json.load(result, cls=JSONDecoder)
        write_transcripts(jobfiles)

    with open(resfile, "rt", encoding="utf-8") as result:
        jobfiles = json.load(result, cls=JSONDecoder)

    preprocessors = [WordReplacer(wipdir)]
    postprocessors = [Tagger(wipdir), Transcriber(wipdir)]
    write_transcripts(jobfiles,
                      json_only=False,
                      preprocessors=preprocessors,
                      postprocessors=postprocessors)

    if "enhance" in kwargs["stages"] and config["output_enhance"]:
        for jobfile in jobfiles:
            infile = wipdir / jobfile.enhancepath.name
            if infile.exists():
                outfile = jobfile.outpath.with_name(
                    (f"{jobfile.outpath.stem}_{jobfile.channel}"
                     f"E{jobfile.enhancepath.suffix}"))
                shutil.copy(str(infile), str(outfile))
                outfile.chmod(0o776)

    for jobfile in jobfiles:
        if jobfile.path.exists():
            outfile = jobfile.outpath.with_suffix(jobfile.path.suffix)
            jobfile.path.rename(outfile)

    logging.info("Job \"%s\" has been completed in Pipeline \"%s\".",
                 wipdir.name, kwargs["pipeline"])
