#!/usr/bin/env python3

from pathlib import Path
from typing import List, Optional
import configparser
import logging
import time

from multiprocess import Manager, Process  # noqa
from redis import Redis

from .worker import WorkerQueue


def poll_work(pipeline: "Pipeline", interval: int):

    logging.debug("Poller \"%s\" has been created successfully!",
                  pipeline.name)

    try:
        while True:
            pipeline.work_on()
            time.sleep(interval)
    except KeyboardInterrupt:

        logging.debug("Poller \"%s\" has been terminated successfully!",
                      pipeline.name)


class Pipeline:

    def __init__(self,
                 confdir: Path,
                 expdir: Path,
                 indir: Path,
                 outdir: Path,
                 wipdir: Path,
                 *,
                 conn: Optional[Redis] = None):

        self.name = confdir.name
        self.dirs = {
            "conf": confdir,
            "exp": expdir,
            "in": indir,
            "out": outdir,
            "wip": wipdir
        }

        self.wait_interval = 60
        self.max_files_per_job = 5
        self.num_workers = 1
        self.work_stages: List[str] = []
        self.read_config(confdir / "pipeline.conf")

        self.queue = WorkerQueue(self.name,
                                 self.dirs,
                                 conn=conn,
                                 num_workers=self.num_workers)
        self.queue.start()

        self.lock = Manager().RLock()
        self.datafiles: List[Path] = Manager().list()
        self.poller = Process(target=poll_work,
                              args=(self, self.wait_interval))
        self.poller.start()

    def read_config(self, conffile: Path):
        reader = configparser.ConfigParser()
        reader.read(conffile)

        self.wait_interval = reader.getint("pipeline",
                                           "wait_interval",
                                           fallback=60)
        self.max_files_per_job = reader.getint("pipeline",
                                               "max_files_per_job",
                                               fallback=5)
        self.num_workers = reader.getint("pipeline", "num_workers", fallback=1)

        stages = reader.get("pipeline", "work_stages", fallback="")
        self.work_stages = stages.split(";")

    def work_on(self, srcfile: Optional[Path] = None):
        with self.lock:
            if srcfile is not None:
                self.datafiles.append(srcfile)
                if len(self.datafiles) >= self.max_files_per_job:
                    self.queue.add_job(self.work_stages, self.datafiles)
                    self.datafiles[:] = []
            else:
                if self.datafiles:
                    self.queue.add_job(self.work_stages, self.datafiles)
                    self.datafiles[:] = []

    def close(self):
        self.queue.shutdown()

        logging.debug("Pipeline \"%s\" has been terminated successfully!",
                      self.name)
