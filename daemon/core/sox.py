#!/usr/bin/env python3

import wave

import ffmpeg
import sox

sox.core.VALID_FORMATS.append("WAV")


def is_valid(filename):
    data = wave.open(str(filename), "rb")
    nframes = data.getnframes()
    return sum(data.readframes(nframes)) != 0


def convert(filename1, filename2):
    stream = ffmpeg.input(str(filename1))
    stream = ffmpeg.output(stream, str(filename2))
    ffmpeg.run(stream, quiet=True)
    return filename2


def get_info(filename):
    info = sox.file_info.info(str(filename))
    return (info["bitrate"], info["duration"], info["encoding"],
            info["sample_rate"])


def get_num_channels(filename):
    try:
        return sox.file_info.info(str(filename))["channels"]
    except sox.core.SoxiError:
        return 0


def resample(filename1, filename2, *, nbits=16, freq=8000):
    tfm = sox.Transformer()
    tfm.set_globals(dither=False)
    tfm.set_output_format(bits=nbits, rate=freq, encoding="signed-integer")
    tfm.build(str(filename1), str(filename2))
    return filename2


def trim(filename1, filename2, start, end, *, nbits=16, freq=8000):
    tfm = sox.Transformer()
    tfm.set_globals(dither=False)
    tfm.set_output_format(bits=nbits, rate=freq, encoding="signed-integer")
    tfm.trim(start_time=start, end_time=end)
    tfm.build(str(filename1), str(filename2))
    return filename2


def extract_channel(filename1, filename2, channel, *, nbits=16, freq=8000):
    tfm = sox.Transformer()
    tfm.set_globals(dither=False)
    tfm.set_output_format(bits=nbits, rate=freq, encoding="signed-integer")
    tfm.remix(remix_dictionary={1: [channel]})
    tfm.build(str(filename1), str(filename2))
    return filename2
