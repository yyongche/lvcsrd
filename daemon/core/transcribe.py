#!/usr/bin/env python3

import json


class Transcriber:

    def __init__(self, confdir):
        self.conf = {}
        self._enable = False

        configfile = confdir / "@lvcsr.transcriber.json"
        if configfile.exists():
            with open(str(configfile), "rt", encoding="utf-8") as config:
                try:
                    self.conf = json.load(config)
                    self.init_config()
                    self._enable = True
                except json.JSONDecodeError:
                    self._enable = False

    @property
    def enable(self):
        return self._enable

    @property
    def suffix(self):
        return ".transcriber.txt"

    def init_config(self):
        if "len1word" not in self.conf:
            self.conf["len1word"] = False
        if "bwords" not in self.conf:
            self.conf["bwords"] = {}
        for param in ["fillers", "spwords"]:
            if param not in self.conf:
                self.conf[param] = []

    def write_segment(self, text, segment):
        for word in segment:
            if word.ident in self.conf["fillers"]:
                text = text + f"%{word.ident} "
                continue
            if word.ident in self.conf["spwords"]:
                text = text + f"{{#{word.ident}}} "
                continue

            new_word = word.ident
            if self.conf["len1word"] and len(word.ident) == 1:
                new_word = f"{{*{word.ident}}}"
            elif word.ident.startswith("{"):
                if word.ident in self.conf["bwords"]:
                    new_word = self.conf["bwords"][word.ident]
            text = text + f"{new_word} "
        return text

    def merge_tokens(self, text):
        text = text.replace("{*I} {#", "{#I} {#")
        tokens = text.split(" ")
        text = f"{tokens[0]} "
        for i in range(1, len(tokens)):
            j = i - 1
            if tokens[j].startswith("{*"):
                if tokens[i].startswith("{*"):
                    text = text[:-2] + f"{tokens[i][2:]} "
                else:
                    text = text + f"{tokens[i]} "
            elif tokens[j].startswith("{#"):
                if tokens[i].startswith("{#"):
                    text = text[:-2] + f" {tokens[i][2:]} "
                else:
                    text = text + f"{tokens[i]} "
            else:
                text = text + f"{tokens[i]} "
        return text

    def write_text(self, obj, outfile):
        with open(str(outfile), "wt", encoding="utf-8",
                  newline='\r\n') as content:
            for _, results in obj["channels"].items():
                text = ""
                for segment in results["stt"]:
                    if segment:
                        text = self.write_segment(text, segment)
                text = self.merge_tokens(text)
                content.write(f"\n{text}\n")
