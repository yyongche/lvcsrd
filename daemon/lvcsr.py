#!/usr/bin/env python3

from pathlib import Path
from typing import Dict, List, Optional
import argparse
import configparser
import logging
import sys

from redis import Redis
from watchdog.observers import Observer
from watchdog.observers.polling import PollingObserver
import redis.exceptions

from config.pipeline import Pipeline as Configline
from core.pipeline import Pipeline
from core.watch import Watcher


def resolve_path(pathname: str) -> Path:
    if pathname.startswith("$HOME/"):
        return Path.home() / pathname[6:]
    return Path(pathname).resolve()


def build_pipelines(confdir: Path,
                    expdir: Path,
                    indir: Path,
                    outdir: Path,
                    wipdir: Path,
                    *,
                    conn: Optional[Redis] = None) -> Dict[str, Pipeline]:

    pipelines = {}
    conffiles = confdir.glob("*/pipeline.conf")
    for conffile in conffiles:

        name = conffile.parent.name
        try:

            inputdir = indir / name
            inputdir.mkdir(exist_ok=True)
            inputdir.chmod(0o777)
            outputdir = outdir / name
            outputdir.mkdir(exist_ok=True)
            outputdir.chmod(0o777)

            pipeline = Pipeline(conffile.parent,
                                expdir,
                                inputdir,
                                outputdir,
                                wipdir,
                                conn=conn)

            pipelines[name] = pipeline

            logging.debug("Pipeline \"%s\" has been configured successfully!",
                          name)

        except configparser.Error:

            logging.exception("Configuration error in Pipeline \"%s\"!", name)

    return pipelines


def build_configline(confdir: Path,
                     expdir: Path,
                     wipdir: Path,
                     pipelines: List[str],
                     *,
                     conn: Optional[Redis] = None) -> Configline:

    configline = Configline(confdir, expdir, wipdir, pipelines, conn=conn)

    logging.debug("Pipeline \"Config\" has been created successfully!")

    return configline


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--conffile", type=str, default="conf/lvcsr.conf")
    args = parser.parse_args()

    _conn = Redis()
    _confdir = resolve_path(args.conffile).parent

    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(asctime)s:%(process)d][%(levelname)s] %(message)s",
        filename=str(_confdir / "lvcsr.log"))
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(
        logging.Formatter("[%(asctime)s][%(levelname)s] %(message)s"))
    logging.getLogger("").addHandler(console)

    try:

        reader = configparser.ConfigParser()
        reader.read(resolve_path(args.conffile))
        _expdir = resolve_path(reader.get("lvcsr", "expdir"))
        _indir = resolve_path(reader.get("lvcsr", "indir"))
        _outdir = resolve_path(reader.get("lvcsr", "outdir"))
        _wipdir = resolve_path(reader.get("lvcsr", "wipdir"))
        extensions = reader.get("lvcsr", "extensions", fallback="*.wav")
        cifs = reader.getboolean("lvcsr", "cifs", fallback=False)

        _conn.ping()

    except configparser.Error:

        logging.exception("Configuration error in Daemon!")
        logging.debug("Daemon has been terminated with failure!")
        sys.exit(1)

    except redis.exceptions.ConnectionError:

        logging.exception("Redis connection error in Daemon!")
        logging.debug("Daemon has been terminated with failure!")
        sys.exit(1)

    _pipelines = build_pipelines(_confdir,
                                 _expdir,
                                 _indir,
                                 _outdir,
                                 _wipdir,
                                 conn=_conn)

    _configline = build_configline(_confdir,
                                   _expdir,
                                   _wipdir,
                                   list(_pipelines.keys()),
                                   conn=_conn)

    if _pipelines:

        observer = Observer() if not cifs else PollingObserver()
        watcher = Watcher(extensions.split(";"), _indir, _pipelines)
        observer.schedule(watcher, str(_indir), recursive=True)
        observer.start()
        try:
            while observer.is_alive():
                observer.join(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()

    else:

        logging.warning("No pipeline has been configured!")

    for _pipeline in _pipelines.values():
        _pipeline.close()
    _configline.close()

    logging.debug("Daemon has been terminated successfully!")
