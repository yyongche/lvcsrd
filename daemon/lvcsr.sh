#!/usr/bin/env bash

trap "exit 0" SIGINT

if [ "$#" -eq 0 ]; then

    python3 $APPDIR/usr/src/daemon/lvcsr.py \
        --conffile $HOME/conf/lvcsr.conf

elif [ "$#" -ge 1 ]; then

    if [ "$1" == "version" ]; then

        echo ${APP_VERSION}
    
    elif [ "$1" == "rq" ]; then

        shift
        rq $@
    
    fi

fi
