#!/usr/bin/env python3

from pathlib import Path
import json


class Utterance:

    def __init__(self, **kwargs):
        self.phrase = kwargs["phrase"]
        self.source = kwargs["source"] if "source" in kwargs else ""
        self.remark = kwargs["remark"]

    def obj_to_json(self):
        return {
            "__type__": "utterance",
            "phrase": self.phrase,
            "source": self.source,
            "remark": self.remark
        }


class Word:

    def __init__(self, **kwargs):
        self.ident = kwargs["ident"] if "ident" in kwargs else kwargs["word"]
        self.word = kwargs["word"]
        self.begin = kwargs["begin"]
        self.duration = kwargs["duration"]
        self.score = kwargs["score"]

    def obj_to_json(self):
        return {
            "__type__": "word",
            "ident": self.ident,
            "word": self.word,
            "begin": self.begin,
            "duration": self.duration,
            "score": self.score
        }


class Keyword:

    def __init__(self, **kwargs):
        self.keyword = kwargs["keyword"]
        self.begin = kwargs["begin"]
        self.duration = kwargs["duration"]
        self.score = kwargs["score"]

    def obj_to_json(self):
        return {
            "__type__": "keyword",
            "keyword": self.keyword,
            "begin": self.begin,
            "duration": self.duration,
            "score": self.score
        }


class JobFile:

    def __init__(self, **kwargs):
        self.name = kwargs["name"]
        self.pipeline = kwargs["pipeline"]

        self.path = self.path_to_obj(
            kwargs["path"]) if "path" in kwargs else None
        self.outpath = self.path_to_obj(
            kwargs["outpath"]) if "outpath" in kwargs else None
        self.datapath = self.path_to_obj(
            kwargs["datapath"]) if "datapath" in kwargs else None
        self.enhancepath = self.path_to_obj(
            kwargs["enhancepath"]) if "enhancepath" in kwargs else None

        self.filename = self.datapath
        self.channel = kwargs["channel"]
        self.segments = kwargs["segments"] if "segments" in kwargs else []
        self.words = kwargs["words"] if "words" in kwargs else []
        self.keywords = kwargs["keywords"] if "keywords" in kwargs else []
        self.utterances = kwargs[
            "utterances"] if "utterances" in kwargs else []

    @staticmethod
    def path_to_obj(path_str):
        if path_str is not None:
            if isinstance(path_str, (Path, str)):
                return Path(path_str)
        return None

    @staticmethod
    def path_to_json(path_obj):
        if path_obj is not None:
            if isinstance(path_obj, (Path, str)):
                return str(path_obj)
        return None

    def obj_to_json(self):
        return {
            "__type__": "jobfile",
            "name": self.name,
            "pipeline": self.pipeline,
            "path": self.path_to_json(self.path),
            "outpath": self.path_to_json(self.outpath),
            "datapath": self.path_to_json(self.datapath),
            "filename": self.path_to_json(self.filename),
            "channel": self.channel,
            "enhancepath": self.path_to_json(self.enhancepath),
            "segments": self.segments,
            "words": self.words,
            "keywords": self.keywords,
            "utterances": self.utterances
        }


class JSONEncoder(json.JSONEncoder):

    def default(self, o):
        if hasattr(o, "obj_to_json"):
            return o.obj_to_json()
        return json.JSONEncoder.default(self, o)


class JSONDecoder(json.JSONDecoder):

    def __init__(self):
        json.JSONDecoder.__init__(self, object_hook=self.json_to_obj)
        self.typename_to_obj = {
            "jobfile": JobFile,
            "keyword": Keyword,
            "word": Word,
            "utterance": Utterance
        }

    def json_to_obj(self, json_str):
        if "__type__" not in json_str:
            return json_str
        return self.typename_to_obj[json_str["__type__"]](**json_str)
