#!/usr/bin/env python3

import json
import logging

from core import docker


def save_expdir(_, conn, result, *_args, **_kwargs):
    (storesfile, pipeline, stage, expdir) = result
    if stage in CONFIG_STAGES:
        conn.hset(f"{pipeline}_store", f"{stage}_expdir", expdir)

    stores = {pipeline: {}}
    if storesfile.exists():
        with open(str(storesfile), "rt", encoding="utf-8") as database:
            stores = json.load(database)
            if pipeline not in stores:
                stores[pipeline] = {}
    stores[pipeline][f"{stage}_expdir"] = expdir
    with open(str(storesfile), "wt", encoding="utf-8") as database:
        json.dump(stores, database)


def run_job(**kwargs):
    wipdir = kwargs["wipdir"]
    expdir = kwargs["expdir"]

    stage = kwargs["stage"]
    if stage in CONFIG_STAGES:
        config_func = CONFIG_STAGES[stage]
        config_func(kwargs["basedir"], None, wipdir, expdir)

    logging.info("Job \"%s\" has been completed for Pipeline \"%s\".",
                 wipdir.name, kwargs["pipeline"])

    return (kwargs["storesfile"], kwargs["pipeline"], stage, expdir.name)


CONFIG_STAGES = {"kws": docker.run_cskwlm}
