#!/usr/bin/env python3

from pathlib import Path
from typing import List, Optional
import json
import logging
import time

from multiprocess import Process  # noqa
from redis import Redis

from .worker import WorkerQueue


def poll_work(pipeline: "Pipeline", conn: Optional[Redis] = None):

    logging.debug("Poller \"Config\" has been created successfully!")

    conn = Redis() if conn is None else conn
    pubsub = conn.pubsub()
    pubsub.subscribe("lvcsrd_config")

    try:
        while True:
            message = pubsub.get_message()
            if message is not None and message["type"] == "message":
                try:
                    data = json.loads(message["data"])
                    if isinstance(data, dict) and "pipeline" in data \
                            and "stage" in data:
                        pipeline.work_on(data["pipeline"], data["stage"])
                except json.JSONDecodeError:
                    pass
            time.sleep(1)
    except KeyboardInterrupt:

        logging.debug("Poller \"Config\" has been terminated successfully!")


class Pipeline:

    def __init__(self,
                 confdir: Path,
                 expdir: Path,
                 wipdir: Path,
                 pipelines: List[str],
                 *,
                 conn: Optional[Redis] = None):

        self.dirs = {"conf": confdir, "exp": expdir, "wip": wipdir}
        self.pipelines = pipelines

        self.conn = Redis() if conn is None else conn
        self.load_store()

        self.worker = WorkerQueue(self.dirs, conn=self.conn)
        self.worker.start()

        self.poller = Process(target=poll_work, args=(self, self.conn))
        self.poller.start()

    def load_store(self):
        storesfile = self.dirs["conf"] / "stores.json"
        if not storesfile.exists():
            return
        with open(str(storesfile), "rt", encoding="utf-8") as database:
            stores = json.load(database)
        for pipeline in self.pipelines:
            if pipeline in stores:
                self.conn.hset(f"{pipeline}_store", mapping=stores[pipeline])

    def save_store(self):
        stores = {}
        for pipeline in self.pipelines:
            if self.conn.exists(f"{pipeline}_store"):
                store = self.conn.hgetall(f"{pipeline}_store")
                stores[pipeline] = {
                    k.decode(): v.decode()
                    for (k, v) in store.items()
                }
        storesfile = self.dirs["conf"] / "stores.json"
        with open(str(storesfile), "wt", encoding="utf-8") as database:
            json.dump(stores, database)

    def work_on(self, pipeline: str, stage: str):
        if pipeline in self.pipelines:
            self.worker.add_job(pipeline, stage)

    def close(self):
        self.save_store()
        self.worker.shutdown()
        self.worker.join()

        logging.debug("Pipeline \"Config\" has been terminated successfully!")
