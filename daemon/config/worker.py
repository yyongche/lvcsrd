#!/usr/bin/env python3

from pathlib import Path
from typing import Dict, Optional
import datetime
import logging
import os
import shutil
import uuid

from multiprocess import Process  # noqa
from redis import Redis
from rq import Queue, Worker
from rq.command import send_shutdown_command

from .job import run_job, save_expdir


class WorkerQueue(Process):

    def __init__(self, dirs: Dict[str, Path], *, conn: Optional[Redis] = None):
        super().__init__()
        self.dirs = dirs

        self.conn = Redis() if conn is None else conn
        self.queue = Queue(connection=self.conn)
        self.worker = Worker([self.queue],
                             connection=self.conn,
                             log_job_description=False)

    def run(self):
        self.worker.work(logging_level="WARNING")

    def shutdown(self):
        send_shutdown_command(self.conn, self.worker.name)

    def add_job(self, pipeline: str, stage: str):
        currtime = datetime.datetime.now()
        wipname = f"data_{os.getpid()}_{currtime:%d%m%Y%H%M%S%f}"

        logging.info("Job \"%s\" has been created for Pipeline \"%s\".",
                     wipname, pipeline)

        currdir = self.dirs["wip"] / wipname
        currdir.mkdir(mode=0o775)

        confdir = self.dirs["conf"] / pipeline
        conffiles = confdir.glob("*.conf")
        for conffile in conffiles:
            shutil.copy(str(conffile),
                        str(currdir / f"@lvcsr.{conffile.name}"))
        jsonfiles = confdir.glob("*.json")
        for jsonfile in jsonfiles:
            shutil.copy(str(jsonfile),
                        str(currdir / f"@lvcsr.{jsonfile.name}"))

        miscfiles = ["kwfile.txt"]
        for miscname in miscfiles:
            miscfile = confdir / miscname
            if miscfile.exists():
                shutil.copy(str(miscfile),
                            str(currdir / f"@lvcsr.{miscfile.name}"))

        storesfile = self.dirs["conf"] / "stores.json"
        basedir = self.dirs["exp"]
        expdir = self.dirs["exp"] / f"exp_{uuid.uuid4().hex}"
        expdir.mkdir(mode=0o775)

        self.queue.enqueue(run_job,
                           kwargs={
                               "storesfile": storesfile,
                               "pipeline": pipeline,
                               "stage": stage,
                               "basedir": basedir,
                               "expdir": expdir,
                               "wipdir": currdir
                           },
                           on_success=save_expdir,
                           job_timeout=3600)
