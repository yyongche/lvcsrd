#!/usr/bin/env python3

from pathlib import Path
import configparser
import json

from rq import Queue, Worker


class Pipeline:

    def __init__(self, name, work_stages):
        self.name = name
        self.work_stages = work_stages
        self.workers = []
        self.num_jobs = 0


def get_pipelines(confdir):
    pipelines = {}
    conffiles = Path(confdir).glob("*/pipeline.conf")
    for conffile in conffiles:
        pipeline = _read_pipeline_conf(conffile)
        pipelines[pipeline.name] = pipeline

    workers, queues = _get_pipelines_rq()
    for worker in workers:
        if worker["queues"] and worker["queues"][0] in pipelines:
            qname = worker["queues"][0]
            working = bool(worker["state"] != "idle")
            pipelines[qname].workers.append(working)
    for queue in queues:
        if queue["name"] in pipelines:
            pipelines[queue["name"]].num_jobs = queue["count"]
    return list(pipelines.values())


def _read_pipeline_conf(conffile):
    reader = configparser.ConfigParser()
    reader.read(conffile)
    stages = reader.get("pipeline", "work_stages", fallback="")
    pipeline = Pipeline(conffile.parent.name, stages.split(";"))
    return pipeline


def _get_pipelines_rq():

    def serialize_queues(queues):
        return [dict(
            name=q.name,
            count=q.count,
        ) for q in queues]

    def serialize_queue_names(worker):
        return [q.name for q in worker.queues]

    workers = sorted(
        (dict(
            name=worker.name,
            queues=serialize_queue_names(worker),
            state=str(worker.get_state()),
        ) for worker in Worker.all()),
        key=lambda w: (w["state"], w["queues"], w["name"]),
    )
    queues = serialize_queues(sorted(Queue.all()))
    return workers, queues


def config_pipeline(conn, pipeline, stage):
    data = {"pipeline": pipeline, "stage": stage}
    return conn.publish("lvcsrd_config", json.dumps(data))


def has_pipeline(confdir, pipeline):
    pipelinedir = Path(confdir) / pipeline
    pipelineconf = pipelinedir / "pipeline.conf"
    return (pipelinedir.exists() and pipelineconf.exists())


def is_kws_configurable(confdir, pipeline):
    pipelineconf = Path(confdir) / pipeline / "pipeline.conf"
    if pipelineconf.exists():
        pline = _read_pipeline_conf(pipelineconf)
        return ("kws" in pline.work_stages
                and has_kwfile_txt(confdir, pipeline))
    return False


def has_kwfile_txt(confdir, pipeline):
    kwfile_txt = Path(confdir) / pipeline / "kwfile.txt"
    return kwfile_txt.exists()


def read_kwfile_txt(confdir, pipeline):
    keywords = []
    kwfile_txt = Path(confdir) / pipeline / "kwfile.txt"
    if kwfile_txt.exists():
        with open(kwfile_txt, "rt", encoding="utf-8") as kwfile:
            for line in kwfile.readlines():
                values = line.strip().split("\t")
                if len(values) == 2:
                    keywords.append({"word": values[0], "phones": values[1]})
    return keywords


def write_kwfile_txt(confdir, pipeline, keywords):
    kwfile_txt = Path(confdir) / pipeline / "kwfile.txt"
    if kwfile_txt.exists():
        with open(kwfile_txt, "wt", encoding="utf-8") as kwfile:
            for keyword in keywords:
                kwfile.write(f"{keyword['word']}\t{keyword['phones']}\n")
