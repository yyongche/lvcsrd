#!/usr/bin/env python3

import re

from flask import current_app, request
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from marshmallow import Schema, ValidationError, fields, validates_schema
from redis_sentinel_url import connect as from_url
from rq import push_connection, pop_connection

from core import daemon

blp = Blueprint("config", __name__, description="configure the daemon")

WORD_RULES = r"^[^\s\d]+$"
PHN_LIST = [
    "a", "ai", "au", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
    "m", "n", "ng", "ny", "o", "oi", "p", "r", "s", "t", "u", "w", "y"
]


@blp.before_request
def push_rq_connection():
    new_instance_number = request.view_args.get("instance_number")
    if new_instance_number is not None:
        redis_url = current_app.config.get("RQ_DASHBOARD_REDIS_URL")
        if new_instance_number < len(redis_url):
            _, new_instance = from_url(redis_url[new_instance_number])
        else:
            raise LookupError("Index exceeds RQ list. Not Permitted.")
    else:
        new_instance = current_app.redis_conn
    push_connection(new_instance)
    current_app.redis_conn = new_instance


@blp.teardown_request
def pop_rq_connection(_):
    pop_connection()


class KeywordSchema(Schema):
    word = fields.String()
    phones = fields.String()

    @validates_schema
    def validate_phones(self, data, **_):
        if re.match(WORD_RULES, data["word"]) is None:
            raise ValidationError((f"Word \"{data['word']}\" contains spaces "
                                   f"or digits."))
        phns = data["phones"].split()
        rogue_phns = [phn for phn in phns if phn not in PHN_LIST]
        if rogue_phns:
            raise ValidationError((f"Word \"{data['word']}\" contains rogue "
                                   f"phones \"{'.'.join(rogue_phns)}\"."))


@blp.route("/keywords/<pipeline>")
class Keywords(MethodView):

    @blp.response(200, KeywordSchema(many=True))
    def get(self, pipeline):
        confdir = current_app.config.get("LVCSR_DAEMON_CONFDIR", "../conf")
        if not daemon.has_pipeline(confdir, pipeline):
            abort(404, message=f"pipeline \"{pipeline}\" not found")
        if not daemon.is_kws_configurable(confdir, pipeline):
            abort(404,
                  message=(f"\"kws\" for pipeline "
                           f"\"{pipeline}\" is not configurable"))
        keywords = daemon.read_kwfile_txt(confdir, pipeline)
        return keywords

    @blp.arguments(KeywordSchema(many=True))
    @blp.response(204)
    def put(self, keywords, pipeline):
        confdir = current_app.config.get("LVCSR_DAEMON_CONFDIR", "../conf")
        if not daemon.has_pipeline(confdir, pipeline):
            abort(404, message=f"pipeline \"{pipeline}\" not found")
        if not daemon.is_kws_configurable(confdir, pipeline):
            abort(404,
                  message=(f"\"kws\" for pipeline "
                           f"\"{pipeline}\" is not configurable"))
        daemon.write_kwfile_txt(
            confdir, pipeline,
            keywords if len(keywords) < 50 else keywords[:50])


@blp.route("/daemon/<pipeline>/<stage>")
class Daemon(MethodView):

    @blp.response(204)
    def put(self, pipeline, stage):
        confdir = current_app.config.get("LVCSR_DAEMON_CONFDIR", "../conf")
        if not daemon.has_pipeline(confdir, pipeline):
            abort(404, message=f"pipeline \"{pipeline}\" not found")
        if stage not in ["kws"]:
            abort(404,
                  message=(f"\"{stage}\" for pipeline "
                           f"\"{pipeline}\" is not configurable"))
        if stage == "kws" and not daemon.is_kws_configurable(
                confdir, pipeline):
            abort(404,
                  message=(f"\"kws\" for pipeline "
                           f"\"{pipeline}\" is not configurable"))
        res = daemon.config_pipeline(current_app.redis_conn, pipeline, stage)
        if res == 0:
            abort(503, message="daemon is not available for configure")
