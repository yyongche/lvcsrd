#!/usr/bin/env python3

import os

from flask import Flask
from flask_smorest import Api
import rq_dashboard

from config import blp as config_blp
from dashboard import blp as dashboard_blp

app = Flask(__name__, static_url_path="/static", static_folder="static")
app.config.from_object(rq_dashboard.default_settings)
app.config["API_TITLE"] = "LVCSR Daemon API"
app.config["API_VERSION"] = "0.1"
app.config["OPENAPI_VERSION"] = "3.0.3"
app.config["OPENAPI_URL_PREFIX"] = "/api"
app.config["OPENAPI_RAPIDOC_PATH"] = "/ui"
app.config["OPENAPI_RAPIDOC_URL"] = "/static/rapidoc-min.js"

if "RQ_DASHBOARD_REDIS_URL" in os.environ:
    redis_url = os.environ["RQ_DASHBOARD_REDIS_URL"]
    app.config["RQ_DASHBOARD_REDIS_URL"] = redis_url
if "LVCSR_DAEMON_CONFDIR" in os.environ:
    config_dir = os.environ["LVCSR_DAEMON_CONFDIR"]
    app.config["LVCSR_DAEMON_CONFDIR"] = config_dir

app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")
app.register_blueprint(dashboard_blp)

api = Api(app)
api.register_blueprint(config_blp, url_prefix="/config")

if __name__ == "__main__":
    app.run()
