#!/usr/bin/env python3

import datetime

from flask import Blueprint, current_app, request, render_template
from redis_sentinel_url import connect as from_url
from rq import push_connection, pop_connection

from core import daemon

blp = Blueprint("dashboard", __name__)


@blp.before_request
def push_rq_connection():
    new_instance_number = request.view_args.get("instance_number")
    if new_instance_number is not None:
        redis_url = current_app.config.get("RQ_DASHBOARD_REDIS_URL")
        if new_instance_number < len(redis_url):
            _, new_instance = from_url(redis_url[new_instance_number])
        else:
            raise LookupError("Index exceeds RQ list. Not Permitted.")
    else:
        new_instance = current_app.redis_conn
    push_connection(new_instance)
    current_app.redis_conn = new_instance


@blp.teardown_request
def pop_rq_connection(_):
    pop_connection()


@blp.route("/")
def index():
    icons = [
        "nes-mario", "nes-ash", "nes-pokeball", "nes-bulbasaur",
        "nes-charmander", "nes-squirtle", "nes-kirby"
    ]

    confdir = current_app.config.get("LVCSR_DAEMON_CONFDIR", "../conf")
    pipelines = daemon.get_pipelines(confdir)

    plines, picons = [], []
    for i, pipeline in enumerate(pipelines):
        if len(pipeline.name) > 16:
            pipeline.name = f"{pipeline.name[:16]} ..."
        plines.append(pipeline)
        picons.append(icons[i % len(icons)])

    return render_template("dashboard.html",
                           pipelines=plines,
                           icons=picons,
                           last_updated=datetime.datetime.now())
