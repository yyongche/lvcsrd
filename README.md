# README

## Packaging

### Packaging Daemon

```
cd daemon
pipenv shell
cd ..
appimage-builder --recipe AppImageBuilder.yml --skip-test
```

### Packaging API

```
cd api
docker build -t lvcsrd-api:0.1 .
docker save lvcsrd-api:0.1 | gzip > ../lvcsrd-api.tar.gz
```

## Development

### Running Daemon

```
cd daemon
pipenv shell
cd ..
./tests/start_redis.sh
python daemon/lvcsr.py
./tests/stop_redis.sh
```

### Running API

```
cd api
pipenv shell
FLASK_APP=app/main flask run
```
